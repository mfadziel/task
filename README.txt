Readme
======

How to compile & run the utility
--------------------------------

Substitute JAVA_HOME with your JVM installation and you can build & run
the utility like this:

	export JAVA_HOME=/usr/lib/jvm/java-7-oracle
	$JAVA_HOME/bin/javac -cp lib/opencsv-3.7.jar -d bin -sourcepath src src/com/egnyte/utils/auditreporter/Runner.java src/com/egnyte/utils/auditreporter/ExtendedRunner.java
	$JAVA_HOME/bin/java -cp bin:lib/opencsv-3.7.jar com.egnyte.utils.auditreporter.Runner resources/users.csv resources/files.csv --top 3 -c

	
Command Line parameters
-----------------------

It is demanded that you first give the location of CSV file with information about users and after that the location of file with information about files.
After that there come optional parameters. Type '-c' for CSV output and '--top n' for top n biggest files. You can give those two parameters together
or separately, the order doesn't matter. The important thing is to give 'n' parameter right after '--top' and for 'n' to be an integer value.
If n is bigger then the total number of files, you will get all the files sorted by their size descending.
The utility takes the first occurence of optional parameter e.g. for input '--top 3 --top 5' it will return top 3 biggest files.


External Libraries
------------------

I used OpenCSV 3.7 library to create CSV files and JUnit 4.12 for testing. All jars can be found in /lib directory.


Test Code
---------

Test code can be found in /test/com/egnyte/utils/auditreporter/ directory. I used Eclipse IDE to run this test class.
I was testing for errors caused by incorrect command line parameters.