package com.egnyte.utils.auditreporter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.opencsv.CSVWriter;

public class ExtendedRunner {
	
	private boolean csvEnabled;
	private boolean topEnabled;
	private int nTop;
	
	public ExtendedRunner() {
		csvEnabled = false;
		topEnabled = false;
		nTop = -1;
	}
	
	public boolean run(String[] args, List<List<String>> users, List<List<String>> files) {
		if (users == null || files == null) {
			return false;
		}
		
		parseArgs(args);
		if (!(csvEnabled || topEnabled)) {
			return false;			
		}
		
		List<String[]> data = new ArrayList<String[]>();
		data = refactorData(users, files);
		outputData(data);
		return true;
	}
	
	private void parseArgs(String[] args) {
    	int index = -1;
    	csvEnabled = false;
    	topEnabled = false;
    	nTop = -1;
    	if (Arrays.asList(args).contains("-c")) {
    		csvEnabled = true;
    	}
    	if ((index = Arrays.asList(args).indexOf("--top")) > -1) {
    		try {
    			nTop = Integer.valueOf(args[index+1]);
    			if (nTop >= 0){
        			topEnabled = true;
    			}
    		} catch (ArrayIndexOutOfBoundsException eArrayIndex) {
    			System.out.println("!! You must specify the value of n.");
    		} catch (NumberFormatException eNumberFormat) {
    			System.out.println("!! After --top specify the value of n (has to be an integer value).");
    		}
    	}
	}
	
	private List<String[]> refactorData(List<List<String>> users, List<List<String>> files) {
		List<String[]> data = new ArrayList<String[]>();    	
        for (List<String> userRow : users) {
            long userId = Long.parseLong(userRow.get(0));
            String userName = userRow.get(1);
            
            for (List<String> fileRow : files) {
                long size = Long.parseLong(fileRow.get(1));
                String fileName = fileRow.get(2);
                long ownerUserId = Long.parseLong(fileRow.get(3));
                if (ownerUserId == userId) {
                		data.add(new String[] {userName, fileName, String.valueOf(size)});             	
                }
            }
        }
        
        if (topEnabled){
        	data = trimData(data);
        	if (csvEnabled) {
        		data = reorganizeDataCols(data);
        	}
        }
        
		return data;
	}
	
	private List<String[]> trimData(List<String[]> data) {
		data.sort(Comparator.comparing(a -> -Integer.parseInt(a[2])));
    	if (nTop > data.size()) {
    		System.out.println("!! "+ nTop + " is bigger than the total number of files.");
    		nTop = data.size();
    	}
    	data = data.subList(0, nTop);		
		return data;
	}
	
	private List<String[]> reorganizeDataCols(List<String[]> data) {
		List<String[]> newData = new ArrayList<String[]>();
		for (String[] dataRow: data){
			newData.add(new String[]{dataRow[1],dataRow[0],dataRow[2]});			
		}		
		return newData;
	}
	
	private void outputData(List<String[]> data) {
		if (csvEnabled) {
				try {
					saveToCSVFile(data);
				} catch (IOException e) {
					System.out.println("!! Couldn't save report to CSV file.");
				}
		}
		else {
			if (topEnabled) {
				printTopFiles(data);
			}
		}
	}
	
	private void saveToCSVFile(List<String[]> data) throws IOException {
    	CSVWriter csvWriter = null;
    	
    	try {
        	csvWriter = new CSVWriter(new FileWriter("report.csv"));
            csvWriter.writeAll(data); 		
    	} finally {
    		if(csvWriter != null) {
            	csvWriter.close();	
    		}
    	}
    }
	
	private void printTopFiles(List<String[]> data) {
		printHeader();
		for(String[] dataRow: data){
			printFile(dataRow[0], dataRow[1], dataRow[2]);
		}
	}
	
    private void printHeader() {
    	System.out.println("Top #" + nTop + " Report");
        System.out.println("============");
    }

    private void printFile(String fileName, String userName, String fileSize) {
    	System.out.println("* " + fileName + " ==> " + "user "+ userName + ", "+ fileSize + " bytes");
    }


}
