package com.egnyte.utils.auditreporter;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class ExtendedRunnerTest {
	
	private ExtendedRunner er;
	private List<List<String>> users;
	private List<List<String>> files;
	
	@Before
	public void setUp() {
		er = new ExtendedRunner();
		String[] user = {"1","userName1"};
		users = new ArrayList<List<String>>();
		users.add(Arrays.asList(user));
		
		String[] file = {"b4f3eecf-95aa-42a7-bffc-83a5441b7d2e", "734003200", "movie.avi", "1"};
		files = new ArrayList<List<String>>();
		files.add(Arrays.asList(file));
	}

	@Test
	public void testRunGoodInput() {
		String[] args = {"--top","10", "-c"};
		assertTrue(er.run(args, users, files));
	}
	
	@Test
	public void testRunMissingN() {
		String[] args = {"--top"};
		assertFalse(er.run(args, users, files));
	}
	
	@Test
	public void testRunWrongOrder() {
		String[] args = {"3", "--top"};
		assertFalse(er.run(args, users, files));
	}
	
	@Test
	public void testRunMultipleCSVParam() {
		String[] args = {"-c", "-c", "-c"};
		assertTrue(er.run(args, users, files));
	}
	
	@Test
	public void testRunMultipleTopParamFirstOK() {
		String[] args = {"--top", "9", "--top", "19", "--top", "99"};
		assertTrue(er.run(args, users, files));
	}

	@Test
	public void testRunMultipleTopParamFirstNotOK() {
		String[] args = {"--top", "--top", "19", "--top", "99"};
		assertFalse(er.run(args, users, files));
	}
	
	@Test
	public void testRunWrongNumberFormat() {
		String[] args = {"--top", "9,1"};
		assertFalse(er.run(args, users, files));
	}

}
